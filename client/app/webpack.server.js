const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
	mode: 'production',
	entry: './server.js',
	output: {
		path: path.resolve(process.cwd(), 'build'),
		filename: 'server.js'
	},
	target: 'node',
	externals: [nodeExternals()],
	resolve: {
		modules: [process.cwd(), 'node_modules']
	},
	module: {
	    rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				}
			}
		]
	},
	optimization: {
		minimize: false
	},
	devtool: 'inline-source-map',
	plugins: [
    	new webpack.DefinePlugin({
			__IS_SERVER: true,
			__API_URL: process.env.API_URL_SERVERSIDE ? 
				JSON.stringify(process.env.API_URL_SERVERSIDE) : 
				JSON.stringify('http://api:8000'),
		}),
		new CleanWebpackPlugin([path.resolve(process.cwd(), 'build', 'server.js')]),
	]
}