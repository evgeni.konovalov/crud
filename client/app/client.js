import React from 'react'
import ReactDOM from 'react-dom'

import {getStoreWithRunningSagas} from 'store'

import App from 'components/App'

const container = document.getElementById('app')

const store = getStoreWithRunningSagas(window.__INITIAL_STATE)
ReactDOM.hydrate(<App store={store}/>, container)