const merge = require('webpack-merge')
const LiveReloadPlugin = require('webpack-livereload-plugin')

const clientConfig = require('./webpack.client.js')
const serverConfig = require('./webpack.server.js')

const clientConfigDev = merge(clientConfig, {
    mode: 'development',
    output: {
        publicPath: '/',
        filename: 'js/[name].js'
    },
    optimization: {
        splitChunks: false
    },
    devtool: 'inline-source-map',
    plugins: [
        new LiveReloadPlugin()
    ]
})

const serverConfigDev = merge(serverConfig, {
	mode: 'development'
})

module.exports = [serverConfigDev, clientConfigDev]