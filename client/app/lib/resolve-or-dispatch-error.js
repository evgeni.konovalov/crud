import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'

import {error} from 'store/actions/error'

const resolveOrDispatchError = (resolveThis, status, message) => {

	return WrappedComponent => {

		const isLastInChain = !WrappedComponent.isChainsPromises

		@withRouter
		@connect()
		class ResolveOrDispatchError extends React.Component {

			static isChainsPromises = true

			static defaultProps = {prevPromise: Promise.resolve()}

			state = {showSpinner: false}

			constructor(props) {
				super(props)
				this.promise = this.props.prevPromise
			}

			componentWillMount() {

				const {dispatch, staticContext, prevPromise} = this.props
				
				if (!(staticContext && staticContext.promisesAreResolved)) {

					this.promise = this.promise
					.then(() => {
						this.setState({showSpinner: true})
						return resolveThis(this.props)
					})
					.catch(e => {
						console.error(e)
						dispatch(error(status, message))
						if (staticContext && !staticContext.status)
							staticContext.status = status
					})
					.finally(() => {
						this.setState({showSpinner: false})
					})

					if (isLastInChain) {
						if (staticContext && staticContext.promises) 
							staticContext.promises.push(this.promise)
					}

				}
			}

			render() {
				const {showSpinner} = this.state
				return isLastInChain && showSpinner ? 
						<div className="d-flex justify-content-center">
							<i className="fas fa-spinner fa-pulse"></i>
						</div>
					:
						<WrappedComponent {...this.props} prevPromise={this.promise}/>
			}
		}
		return ResolveOrDispatchError

	}

}

export const resolveOr500 = resolveThis => {
	return resolveOrDispatchError(resolveThis, 500, 'Sorry, something went wrong...')
}

export const resolveOr404 = resolveThis => {
	return resolveOrDispatchError(resolveThis)
}

export {resolveOrDispatchError}