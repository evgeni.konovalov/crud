import nodeFetch from 'node-fetch'

var fetch = fetch || nodeFetch

export default url => {
	return fetch(url).then(res => res.json())
}