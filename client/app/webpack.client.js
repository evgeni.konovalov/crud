const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')

module.exports = {
	mode: 'production',
	entry: {
		app: ['./client.js']
	},
	output: {
		path: path.resolve(process.cwd(), 'build', 'bundles'),
		filename: 'js/[name].[chunkhash].js'
	},
	resolve: {
		modules: [process.cwd(), 'node_modules']
	},
	module: {
	    rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				}
			},
		]
	},
	optimization: {
		splitChunks: {
			chunks: "all" 
		}
	},
	devtool: 'source-map',
	plugins: [
		new CleanWebpackPlugin([path.resolve(process.cwd(), 'build', 'bundles')]),
    	new ManifestPlugin({
    		fileName: 'manifest.json',
    		filter: ({name}) => !name.match(/.map$/)
    	}),
		new webpack.HashedModuleIdsPlugin(),
		new webpack.DefinePlugin({
			__IS_SERVER: false,
			__API_URL: process.env.API_URL_CLIENTSIDE ? 
				JSON.stringify(process.env.API_URL_CLIENTSIDE) : 
				JSON.stringify('http://localhost:8000'),
		}),
    ],
}