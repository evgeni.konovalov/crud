import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'

import {fetchTopItems} from 'store/actions/top-items'

import Spinner from 'components/Spinner'
import ItemCard from 'components/ItemCard'

const mapStateToProps = ({topItems, categories: {list: categories}}) => ({topItems, categories})

@withRouter
@connect(mapStateToProps)
export default class TopItems extends React.Component {
	componentWillMount() {
		const {dispatch, match} = this.props
		dispatch(fetchTopItems)
	}
	render() {
		const {topItems, categories} = this.props
		const {items, isFetching} = topItems
		return isFetching ? 
			<Spinner/>
		:
			items.length ?
				<div className="row">
					{items.map((item, i) => {
						const category = categories.find(c => c.id == item.category)
						return <ItemCard {...item} category={category} key={i}/>
					})}
				</div>
			:
				null

	}
}