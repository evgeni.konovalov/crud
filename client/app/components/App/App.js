import React from 'react'
import {BrowserRouter, StaticRouter} from 'react-router-dom'
import {Provider} from 'react-redux'

import RouterErrorSwitch from 'components/RouterErrorSwitch'

const App = ({context, location, store}) => {
	const Router = __IS_SERVER ? StaticRouter : BrowserRouter
	return (
		<Provider store={store}>
			<Router context={context} location={location}>
				<RouterErrorSwitch/>
			</Router>
		</Provider>
	)
}

export default App