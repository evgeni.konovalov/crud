import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'

import {switchCategory} from 'store/actions/category'

import Spinner from 'components/Spinner'
import ItemCard from 'components/ItemCard'

const mapStateToProps = ({category, categories: {list: categories}}) => ({category, categories})

@withRouter
@connect(mapStateToProps)
export default class CategoryList extends React.Component {
	componentWillMount() {
		const {dispatch, match} = this.props
		dispatch(switchCategory(match.params.slug))
	}
	render() {
		const {slug, items, isFetching} = this.props.category
		return isFetching ? 
			<Spinner/>
		:
			items.length ?
				<div className="d-flex flex-column">
					<div></div>
					<div className="row">
						{items.map((item, i) => (
							<ItemCard {...item} key={i}/>
						))}
					</div>
				</div>
			:
				null

	}
}