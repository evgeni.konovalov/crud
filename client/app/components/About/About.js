import React from 'react'

import config from 'config'

import Layout from 'components/Layout'
import Header from 'components/Header'

const About = () => <Layout header={

		<Header subtitle={false} navigation={false}/>

	}>

		<div className="text-justify">

			<p>Greetings.</p>

			<p>
				This is an example of primitive microserviced web application made with <b>ReactJS</b> and common react tools like&nbsp;
				<b>react-router</b>, <b>redux</b>, <b>redux-saga</b> on the front side and <b>Django</b> with <b>rest-framework</b> on the api side,
				developed and delivered with <b>Docker</b> and <b>GitLab</b>. It is contained from the client (this app), the admin 
				(not implemented yet) and <a target="_blank" href="http://api.app.evgeni.konovalov.fvds.ru">the api</a>.
			</p>

			<p>
				You can <a target="_blank" href="https://gitlab.com/evgeni.konovalov/crud.git">check the repo</a> for a code details.
			</p>

			<p>
				I'm a frontend developer with a nice backend skills. Text me or mail:
				<ul>
					<li>Whatsapp: <a href="https://wa.me/79995684023">evgeni konovalov</a></li>
					<li>Telegram: <a href="tg://resolve?domain=evgenynikolaevich">@evgenynikolaevich</a></li>
					<li>Email: <a href="mailto:evgeni.konovalov@gmail.com">evgeni.konovalov@gmail.com</a></li>
				</ul>
			</p>

		</div>

	</Layout>

export default About