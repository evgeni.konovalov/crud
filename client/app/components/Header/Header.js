import React from 'react'

import {Link} from 'react-router-dom'

import Navigation from 'components/Navigation'

const Header = ({subtitle=true, navigation=true, home=true}) => (
	<header>
		<div className="pt-3 small">
			<Link to='/' style={{visibility: home ? 'visible' : 'hidden'}}>Home</Link>
		</div>
		<div className="pt-1 pb-3 py-md-5 d-flex flex-column align-items-center text-center">
			<h1 className="display-1">CRUD</h1>
			{subtitle &&
				<div>
					<p>
						An example of primitive so-called <i>Create, Read, Update, Delete</i> app made with 
						Reactjs. <a target="_blank" href="https://gitlab.com/evgeni.konovalov/crud">Details...</a>
					</p>
				</div>
			}
		</div>
		{navigation &&
			<Navigation/>
		}
	</header>
)

export default Header