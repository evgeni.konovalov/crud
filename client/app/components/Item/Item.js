import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'

import {switchItem} from 'store/actions/item'

import LayoutBusiness from 'components/LayoutBusiness'
import Spinner from 'components/Spinner'

const mapStateToProps = ({categories: {list: categories}, item: {data: item, isFetching}}) => {
	const category = categories.find(c => c.id == item.category)
	const {id, name, description, price, picture, slug} = item
	return ({id, name, description, price, picture, slug, isFetching, category})
}

@withRouter
@connect(mapStateToProps)
export default class Item extends React.Component {
	componentWillMount() {
		const {dispatch, match, history} = this.props
		dispatch(switchItem(match.params.slug))
	}
	render() {
		const {id, name, description, price, picture, slug, isFetching, category} = this.props
		return (
			<LayoutBusiness>
				
				{isFetching ? 
					<Spinner/>
				:
					id ? 
						<div className="text-center mx-auto" style={{maxWidth: '30em'}}>
							<div className="mb-3">
								<div className="mx-auto d-flex justify-content-center align-items-center" 
									 style={{width: '15em', height: '15em', borderRadius: '15em', overflow: 'hidden'}}>
									<img src={picture} style={{height: '100%'}}/>
								</div>
							</div>
							<div className="small">
								<Link to="/">Home</Link> → <Link className="text-capitalize" to={`/categories/${category.slug}`}>{category.name}</Link> →
							</div>
							<h1 className="text-capitalize mb-3 h4"><b>{name}</b></h1>
							<div className="mb-3">{description}</div>
							<div><b>Price: {price}</b></div>
						</div>
					:
						null
				}

			</LayoutBusiness>
		)

	}
}