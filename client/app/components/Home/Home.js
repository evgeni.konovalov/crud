import React from 'react'

import Layout from 'components/Layout'
import Header from 'components/Header'
import TopItems from 'components/TopItems'

const Home = () => <Layout header={

		<Header home={false}></Header>

	}>

		<div className="text-center">
			<div className="h4 mb-4"><b>Top Items</b></div>
			<TopItems/>
		</div>

	</Layout>

export default Home