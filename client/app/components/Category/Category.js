import React from 'react'
import {connect} from 'react-redux'

import LayoutBusiness from 'components/LayoutBusiness'
import CategoryList from 'components/CategoryList'

export default class Category extends React.Component {
	render() {
		const {slug} = this.props.match.params
		return (
			<LayoutBusiness>
		
				<CategoryList key={slug}/>

			</LayoutBusiness>
		)

	}
}