import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {NavLink} from 'react-router-dom'

import {fetchCategories} from 'store/actions/categories'

import Spinner from 'components/Spinner'

const mapStateToProps = ({categories: {list: categories, activeCategorySlug}}) => ({categories, activeCategorySlug})

@withRouter
@connect(mapStateToProps)
export default class Navigation extends React.Component {
	componentWillMount() {
		this.props.dispatch(fetchCategories)
	}
	render() {
		const {categories, activeCategorySlug} = this.props
		const isActive = category => (match, location) => {
			if (category.slug == activeCategorySlug) return true
			if (!match) return false
			return true
		}
		return (
			<div className="d-flex flex-column justify-content-center align-items-center mb-5">
				<div className="alert alert-info small d-inline-block mx-auto mb-3">All content in this app is a Lorem Ipsum and has no sense.</div>
				<div className="mb-2 small">Categories:</div>
				<nav className="btn-group">
					{categories.length ?
						categories.map((category, i) =>

							<NavLink activeClassName="active" 
									 to={`/categories/${category.slug}`} 
									 className="btn btn-outline-primary text-capitalize" 
									 key={i}
									 isActive={isActive(category)}>

								{category.name}

							</NavLink>

						) 
					:
						<Spinner/>
					}
				</nav>
			</div>
		)
	}
}