import React from 'react'
import {Route, Switch} from 'react-router-dom'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'

import {error, releaseError} from 'store/actions/error'
import {LOCATION_CHANGED} from 'store/action-types'

import Navigation from 'components/Navigation'
import Home from 'components/Home'
import About from 'components/About'
import Err from 'components/Err'
import Category from 'components/Category'
import Item from 'components/Item'

@connect()
class NotFound extends React.Component {
	componentWillMount() {
		this.props.dispatch(error())
	}
	render() {
		return null
	}
}

const mapStateToProps = ({error}) => ({error})

@withRouter
@connect(mapStateToProps)
class RouterErrorSwitch extends React.Component {
	shouldComponentUpdate(nextProps) {
		const {dispatch, location} = this.props
		if (location.pathname !== nextProps.location.pathname)
			dispatch({type: LOCATION_CHANGED})
		return true
	}
	componentDidUpdate(prevProps) {
		if (this.props.location.pathname !== prevProps.location.pathname) {
			window.scrollTo(0, 0)
		}
	}
	render() {
		const {error} = this.props
		return (
			<div>
				{error.status ?
					<Err status={error.status} message={error.message}></Err>
				:
					<Switch>
						<Route exact path='/' component={Home}/>
						{/*<Route path='/about' component={About}/>*/}
						<Route exact path='/categories/:slug' component={Category}/>
						<Route path='/items/:slug' component={Item}/>
						<Route component={NotFound}/>
					</Switch>
				}
				
			</div>
		)
	}
}

export default RouterErrorSwitch