import React from 'react'
import {Route} from 'react-router-dom'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'

import {releaseError} from 'store/actions/error'

const mapStateToProps = ({error}) => ({error})

@withRouter
@connect(mapStateToProps)
class Err extends React.Component  {
	componentWillMount() {
		const {staticContext, status} = this.props
		if (staticContext) staticContext.status = status
	}
	render() {
		const {status, message} = this.props
		return (
			<div className="text-center w-100 d-flex align-items-center flex-column justify-content-center" style={{height: '100vh'}}>
				<h1>{status}</h1>
				<div>{message}</div>
				<div><a href="/">Home</a></div>
			</div>
		)
	}
}

export default Err