import React from 'react'

const Spinner = props => (
	<div {...props} className="d-flex justify-content-center">
		<i className="fas fa-spinner fa-pulse"></i>
	</div>
)

export default Spinner