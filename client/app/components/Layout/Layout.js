import React from 'react'

import Header from 'components/Header'

const Footer = () => (
	<div className="text-center py-3">
		<hr/>
		<div><a href="tg://resolve?domain=evgenynikolaevich">@evgenynikolaevich</a></div>
		<div className="small">2018</div>
	</div>
)

const Layout = ({children, header=<Header/>, footer=<Footer/>}) => {

	return (
		<div className="container">
			<header>{header}</header>
			<main>{children}</main>
			<footer>{footer}</footer>
		</div>
	)
}

export default Layout