import React from 'react'

import {Link} from 'react-router-dom'

import Layout from 'components/Layout'
import Navigation from 'components/Navigation'

const Header = () => (
	<div>
		<header className="d-flex justify-content-center align-items-center">
			<div className="py-3 mr-3"><h1 className="h2 mb-0">CRUD</h1></div>
			<div>
				<Link className="px-2" to='/'>Home</Link>
				<a className="px-2 pr-0" target="_blank" href='https://gitlab.com/evgeni.konovalov/crud'>About</a>
			</div>
		</header>
		<Navigation/>
	</div>
)

const LayoutBusiness = ({children}) => {
	return <Layout header={<Header/>}>{children}</Layout>
}

export default LayoutBusiness