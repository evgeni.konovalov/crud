import React from 'react'
import {Link} from 'react-router-dom'

const ItemCard = ({name, description, price, picture, slug, category}) => (
	<div className="col-6 col-md-4 text-center mb-4">
		<Link to={`/items/${slug}`}>
			<div className="mb-3">
					<div className="mx-auto d-flex justify-content-center align-items-center" 
						 style={{width: '7em', height: '7em', borderRadius: '7em', overflow: 'hidden'}}>
						<img src={picture} style={{height: '100%'}}/>
					</div>
			</div>
			<div className="text-capitalize small"><b>{name}</b></div>
		</Link>
		{category.name &&
			<div className="small">Category: <Link to={`/categories/${category.slug}`}>{category.name}</Link></div>
		}
		<div className="mt-2">Price: {price}</div>
	</div>
)

export default ItemCard