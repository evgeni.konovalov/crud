import 'source-map-support/register'

import express from 'express'
import React from 'react'
import reactDOMServer from 'react-dom/server'
import Handlebars from 'handlebars'
import fs from 'fs'
import path from 'path'

import {getStore} from 'store'
import rootSaga from 'store/sagas/root'
import {gentleEndSagas} from 'store/actions/gentle-end'

import App from 'components/App'

const server = express()

// Serve static
server.use(express.static('build/bundles'))

// App middleware
server.get('*', async (req, res) => {

	const location = req.url
	const context = {promises: []}

	const store = getStore()
	const rootSagaTask = store.runSaga(rootSaga)

	// First render to initialize all async processes
	reactDOMServer.renderToString(
		React.createElement(App, {context, location, store})
	)
	context.url && res.redirect(context.url)
	
	store.dispatch(gentleEndSagas)
	await rootSagaTask.done
		
	// Final rendering with async processes done
	const app = reactDOMServer.renderToString(
		React.createElement(App, {context, location, store})
	)
	context.status && res.status(context.status)

	const initialState = store.getState()

	fs.readFile('./layout.handlebars', 'utf8', (err, layout) => {
		if (err) throw err
		let bundles = ['/js/app.js', 'http://localhost:35729/livereload.js']
		if (process.env.NODE_ENV == 'production') {
			const manifest = require('build/bundles/manifest.json')
			bundles = Object.values(manifest).reverse()
		}
		const html = Handlebars.compile(layout)({
			title: 'APP',
			initialState: JSON.stringify(initialState),
			app,
			bundles,
		})
		res.send(html)
	})
})

server.listen(8001, () => console.log('[client-server] Server is running on port 8001'))