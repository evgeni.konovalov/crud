import {
	TOP_ITEMS_REQUEST, 
	TOP_ITEMS_RECEIVE
} from 'store/action-types'

const initialState = {
	isFetching: false,
	items: []
}

export default (state = initialState, action) => {
	switch (action.type) {
		case TOP_ITEMS_REQUEST:
			return {
				...state, 
				isFetching: true
			}
		case TOP_ITEMS_RECEIVE:
			return {
				...state,
				isFetching: false, 
				items: action.data,
			}
		default:
			return state
	}
}