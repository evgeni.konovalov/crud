const initialState = {
	isFetching: false,
	data: {}
}
export default (state = initialState, action) => {
	switch (action.type) {
		case 'ITEM_REQUEST':
			return {
				...state, 
				isFetching: true
			}
		case 'ITEM_RECEIVE':
			return {
				isFetching: false,
				data: action.data
			}
		default:
			return state
	}
}