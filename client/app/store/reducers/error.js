const initialState = {
	status: null,
	message: ''
}

export default (state = initialState, action) => {
	switch (action.type) {
		case 'ERROR_SET':
			let {status = 404, message = "Sorry, can not find this page."} = action
			if (status == 500 && !action.message) message = 'Sorry, something went wrong...'
			return {status, message}
		case 'ERROR_RELEASE':
			return initialState
		default:
			return state
	}
}