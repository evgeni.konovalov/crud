import {combineReducers} from 'redux'

import categories from 'store/reducers/categories'
import category from 'store/reducers/category'
import item from 'store/reducers/item'
import error from 'store/reducers/error'
import topItems from 'store/reducers/top-items'

export default combineReducers({categories, category, item, error, topItems})