const initialState = {
	isFetching: false,
	activeCategorySlug: '',
	list: []
}
export default (state = initialState, action) => {
	switch (action.type) {
		case 'CATEGORIES_REQUEST':
			return {
				...state, 
				isFetching: true
			}
		case 'CATEGORIES_RECEIVE':
			return {
				...state,
				isFetching: false,
				list: action.data
			}
		case 'CATEGORIES_ACTIVE_SET':
			return {
				...state,
				activeCategorySlug: action.slug
			}
		case 'CATEGORIES_ACTIVE_UNSET':
			return {
				...state,
				activeCategorySlug: ''
			}
		default:
			return state
	}
}