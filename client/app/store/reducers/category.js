const initialState = {
	slug: '',
	isFetching: false,
	items: []
}

export default (state = initialState, action) => {
	switch (action.type) {
		case 'CATEGORY_REQUEST':
			return {
				...state, 
				isFetching: true
			}
		case 'CATEGORY_RECEIVE':
			return {
				...state,
				isFetching: false, 
				items: action.items,
				slug: action.slug
			}
		default:
			return state
	}
}