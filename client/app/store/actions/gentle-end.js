import {
	CAN_NOT_END, 
	CAN_END, 
	GENTLE_END
} from 'store/action-types'

export const blockSagaEnd = {
	type: CAN_NOT_END
}
export const unblockSagaEnd = {
	type: CAN_END
}
export const gentleEndSagas = {
	type: GENTLE_END
}