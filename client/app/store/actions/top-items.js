import {
	TOP_ITEMS_FETCH,
	TOP_ITEMS_REQUEST,
	TOP_ITEMS_RECEIVE,
	TOP_ITEMS_ARE_FETCHED,
} from 'store/action-types'

export const fetchTopItems = {
	type: TOP_ITEMS_FETCH
}

export const requestTopItems = {
	type: TOP_ITEMS_REQUEST
}

export const receiveTopItems = data => ({
	type: TOP_ITEMS_RECEIVE,
	data
})

export const topItemsAreFetched = {
	type: TOP_ITEMS_ARE_FETCHED
}