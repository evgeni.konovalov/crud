import {
	CATEGORY_REQUEST,
	CATEGORY_RECEIVE,
	CATEGORY_SWITCH,
} from 'store/action-types'

export const requestCategory = {
	type: CATEGORY_REQUEST
}

export const receiveCategory = (items, slug) => ({
	type: CATEGORY_RECEIVE,
	items,
	slug
})

export const switchCategory = slug => ({
	type: CATEGORY_SWITCH,
	slug
})