import {
	ITEM_REQUEST,
	ITEM_RECEIVE,
	ITEM_SWITCH,
} from 'store/action-types'

export const requestItem = {
	type: ITEM_REQUEST
}

export const receiveItem = data => ({
	type: ITEM_RECEIVE,
	data
})

export const switchItem = slug => ({
	type: ITEM_SWITCH,
	slug
})

