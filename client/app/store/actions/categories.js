import {
	CATEGORIES_FETCH,
	CATEGORIES_REQUEST,
	CATEGORIES_RECEIVE,
	CATEGORIES_ARE_FETCHED,
	CATEGORIES_ACTIVE_SET,
	CATEGORIES_ACTIVE_UNSET,
} from 'store/action-types'

export const fetchCategories = {
	type: CATEGORIES_FETCH
}

export const requestCategories = {
	type: CATEGORIES_REQUEST
}

export const receiveCategories = data => ({
	type: CATEGORIES_RECEIVE,
	data
})

export const categroiesAreFetched = {
	type: CATEGORIES_ARE_FETCHED
}

export const setActiveCategory = slug => ({
	type: CATEGORIES_ACTIVE_SET,
	slug
})

export const unsetActiveCategory = {
	type: CATEGORIES_ACTIVE_UNSET,
}