const setError = (status, message) => ({
	type: 'ERROR_SET',
	status,
	message
})

export const error = (status, message) => (dispatch, getState) => {
	!getState().error.status && dispatch(setError(status, message))
}

export const releaseError = {
	type: 'ERROR_RELEASE'
}