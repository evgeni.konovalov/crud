import {take, put, call, select} from 'redux-saga/effects'
import fetch from 'lib/fetch'

import config from 'config'

import {requestItem, receiveItem} from 'store/actions/item'
import {fetchCategories, setActiveCategory} from 'store/actions/categories'
import {blockSagaEnd, unblockSagaEnd} from 'store/actions/gentle-end'
import {error} from 'store/actions/error'

import {
	CATEGORIES_ARE_FETCHED,
	ITEM_SWITCH
} from 'store/action-types'

export function* switchItem() {
	var {slug} = yield take(ITEM_SWITCH)

	// Fetching categories because it's required for Item component

	yield put(blockSagaEnd)
	yield put(fetchCategories)
	yield take(CATEGORIES_ARE_FETCHED)
	yield put(unblockSagaEnd)
	do {
		var item, haveError

		// Checking if we really need to switch to another item

		const currentItemSlug = yield select(s => s.item.data.slug)
		if (slug != currentItemSlug) {

			// Checking if we have this item loaded

			const itemsFromCategoryComponent = yield select(s => s.category.items)
			const itemsFromTopItemsComponent = yield select(s => s.topItems.items)
			const itemsWeHave = [].concat(itemsFromCategoryComponent, itemsFromTopItemsComponent)
			item = itemsWeHave.find(i => i.slug == slug)
			if (!item) {

				// Fetching item and handling response

				yield put(requestItem)
				const fetchResult = yield call(fetch, config.apiUrls.items + `?slug=${slug}`)
				if (fetchResult.length) {
					item = fetchResult[0]
				} else if (fetchResult.length === 0) {
					console.error(`Can't find item with slug "${slug}"`)
					haveError = true
					yield put(error(404))
				} else {
					haveError = true
					yield put(error(500))
				} 
			}
			item && (yield put(receiveItem(item)))
		} else {
			item = yield select(s => s.item.data)
		}
		// Highlighting item's parent category in the navbar
		
		if (!haveError && item) {
			const categories = yield select(s => s.categories.list)
			const activeCategory = categories.find(c => c.id == item.category)
			yield put(setActiveCategory(activeCategory.slug))
		}

		// Waiting for one more circle
		
		var {slug} = yield take(ITEM_SWITCH)
	} while(true)
}