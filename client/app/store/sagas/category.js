import {take, put, call, select} from 'redux-saga/effects'
import fetch from 'lib/fetch'

import config from 'config'

import {requestCategory, receiveCategory} from 'store/actions/category'
import {fetchCategories} from 'store/actions/categories'
import {blockSagaEnd,unblockSagaEnd} from 'store/actions/gentle-end'
import {error} from 'store/actions/error'

import {
	CATEGORY_SWITCH,
	CATEGORIES_ARE_FETCHED
} from 'store/action-types'

export function* switchCategory() {
	var {slug} = yield take(CATEGORY_SWITCH)

	// Fetching categories because it's required

	yield put(blockSagaEnd)
	yield put(fetchCategories)
	yield take(CATEGORIES_ARE_FETCHED)
	yield put(unblockSagaEnd)
	while(true) {

		// Checking if we really need to switch

		const currentCategorySlug = yield select(s => s.category.slug)
		if (slug != currentCategorySlug) {

			// Checking if requested category is exists

			const categories = yield select(s => s.categories.list)
			if (!categories.find(c => c.slug == slug)) {
				console.error(`Can't find category with slug "${slug}"`)
				yield put(error(404))
			} else {

				// Fetching and handling response

				yield put(requestCategory)
				const items = yield call(fetch, config.apiUrls.items + `?category__slug=${slug}`)
				if (Array.isArray(items)) {
					yield put(receiveCategory(items, slug))
				} else {
					yield put(error(500))
				}
			}
		}

		// Waiting for one more circle

		var {slug} = yield take(CATEGORY_SWITCH)
	}
}