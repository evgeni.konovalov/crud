import {take, put, call} from 'redux-saga/effects'
import fetch from 'lib/fetch'

import config from 'config'

import {requestCategories, categroiesAreFetched, receiveCategories} from 'store/actions/categories'
import {error} from 'store/actions/error'

import {
	CATEGORIES_FETCH
} from 'store/action-types'

export function* fetchCategories() {
	yield take(CATEGORIES_FETCH)
	do {

		// Fetching

		yield put(requestCategories)
		var data = yield call(fetch, config.apiUrls.categories)
		var failed = !data.length

		// Take the process in a loop if error

		if (failed) {
			yield put(error(500))
			yield take(CATEGORIES_FETCH)
		}
	} while(failed)
	yield put(receiveCategories(data))
	while(true) {

		// Sending a finishing signal in a loop for a dependend sagas

		yield put(categroiesAreFetched)
		yield take(CATEGORIES_FETCH)
	}
}