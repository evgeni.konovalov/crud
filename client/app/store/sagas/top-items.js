import {take, put, call} from 'redux-saga/effects'
import fetch from 'lib/fetch'

import config from 'config'

import {requestTopItems, topItemsAreFetched, receiveTopItems} from 'store/actions/top-items'
import {fetchCategories} from 'store/actions/categories'
import {blockSagaEnd,unblockSagaEnd} from 'store/actions/gentle-end'
import {error} from 'store/actions/error'

import {
	TOP_ITEMS_FETCH,
	CATEGORIES_ARE_FETCHED
} from 'store/action-types'

export function* fetchTopItems() {
	yield take(TOP_ITEMS_FETCH)

	// Fetching categories because it's required

	yield put(blockSagaEnd)
	yield put(fetchCategories)
	yield take(CATEGORIES_ARE_FETCHED)
	yield put(unblockSagaEnd)
	do {

		// Fetching

		yield put(requestTopItems)
		var data = yield call(fetch, config.apiUrls.items)
		var failed = !data.length

		// Take the process in a loop if error

		if (failed) {
			yield put(error(500))
			yield take(TOP_ITEMS_FETCH)
		}
	} while(failed)

	// Taking first 6 items
	// TODO: add "limit-offset" pagination api functionality
	yield put(receiveTopItems(data.slice(0,6)))
	yield put(topItemsAreFetched)
}