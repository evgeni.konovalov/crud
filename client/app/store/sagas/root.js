import {all} from 'redux-saga/effects'

import {fetchCategories} from 'store/sagas/categories'
import {fetchTopItems} from 'store/sagas/top-items'
import {switchCategory} from 'store/sagas/category'
import {switchItem} from 'store/sagas/item'
import {locationChangingHandling} from 'store/sagas/location'
import gentleEnd from 'store/sagas/gentle-end'

export default function* root() {
	yield all([
		gentleEnd(),
		fetchCategories(),
		fetchTopItems(),
		switchCategory(),
		switchItem(),
		locationChangingHandling()
	])
}