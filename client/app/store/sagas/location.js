import {takeEvery, put} from 'redux-saga/effects'

import {releaseError} from 'store/actions/error'
import {unsetActiveCategory} from 'store/actions/categories'

import {LOCATION_CHANGED} from 'store/action-types'

export function* locationChangingHandling() {
	yield takeEvery(LOCATION_CHANGED, function*() {
		yield put(releaseError)
		yield put(unsetActiveCategory)
	})
}