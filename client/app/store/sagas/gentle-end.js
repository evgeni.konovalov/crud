import {END} from 'redux-saga'
import {race, take, put} from 'redux-saga/effects'

import {
	GENTLE_END,
	CAN_NOT_END,
	CAN_END,
} from 'store/action-types'

// This saga allows to wait for sagas sequences on the server side before stopping them
// To use it dispatch {type: GENTLE_END} action instead of END

export default function* () {
	let sagasToWaitCount = 0,
		end = false
	do {
		const {canEnd, canNotEnd, gentleEnd} = yield race({
			gentleEnd: take(GENTLE_END),
			canNotEnd: take(CAN_NOT_END),
			canEnd: take(CAN_END),
		})
		if (gentleEnd) end = true
		if (canNotEnd) ++sagasToWaitCount
		if (canEnd) --sagasToWaitCount
	} while(sagasToWaitCount > 0 || !end)
	yield put(END)
}