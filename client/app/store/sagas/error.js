import {takeEvery, put} from 'redux-saga/effects'

import {LOCATION_CHANGED} from 'store/action-types'
import {releaseError} from 'store/actions/error'

export function* releaseErrorOnUrlChange() {
	takeEvery(LOCATION_CHANGED, () => {
		put(releaseError)
	})
}