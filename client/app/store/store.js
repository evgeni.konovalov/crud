import {createStore, applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import rootReducer from 'store/reducers/root'
import rootSaga from 'store/sagas/root'

export const getStore = initialState => {
	const sagaMiddleware = createSagaMiddleware()
	let middlewares = [
		thunkMiddleware,
		sagaMiddleware,
	]
	const middlewaresDev = [
		logger
	]
	if (process.env.NODE_ENV != 'production') {
		middlewares = [...middlewares, ...middlewaresDev]
	}
	return {

		...createStore(
			rootReducer, 
			initialState, 
			applyMiddleware.apply(null, middlewares)
		),
		
		runSaga: sagaMiddleware.run
	}
}

export const getStoreWithRunningSagas = initialState => {
	const store = getStore(initialState)
	store.runSaga(rootSaga)
	return store
}