const apiUrls = {
	base: __API_URL,
	categories: __API_URL + '/categories/',
	items: __API_URL + '/items/'
}

export default {
	apiUrls
}