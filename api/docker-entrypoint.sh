#!/bin/bash

echo '[docker-entrypoint] Executing the entrypoint script...'


# Waiting for database

python <<'EOPYTHON'

import os, sys, time

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

from django.db import connection

try_number = 0
connected = False
while try_number < 10 and not connected:
    try:
        connection.cursor()
    except:
        print('[docker-entrypoint] Not connected to the database, waiting...')
    else:
        connected = True
        print('[docker-entrypoint] Connected to the database...')
    time.sleep(1)
    try_number += 1

if not connected:
    sys.exit(1)

EOPYTHON


# Migrating

python manage.py migrate


# Creating superuser

python <<'EOPYTHON'

import os, sys

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

import django
from django.contrib.auth import get_user_model

django.setup()

User = get_user_model()
if not User.objects.filter(is_superuser=True).exists():
    User.objects.create_superuser('admin', 'admin@example.com', 'password')
    print('[docker-entrypoint] A superuser created. Login: admin, password: password...')

EOPYTHON


# Seeding demo data

python <<'EOPYTHON'

import os, sys

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

import django
django.setup()

from django.core.management import call_command

from webapp.models import Category

if Category.objects.count() == 0:
    call_command('seeddemo')
    print('[docker-entrypoint] Seeding the demo data...')

EOPYTHON


# Collecting static

yes yes | python manage.py collectstatic


# Running

echo "[docker-entrypoint] Running the command..."

eval "$@"