from django.shortcuts import render

from rest_framework import viewsets

from .models import Category, Item
from .serializers import CategorySerializer, ItemSerializer

# Create your views here.

class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    filter_fields = ('slug',)

class ItemViewSet(viewsets.ModelViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    filter_fields = ('category', 'category__slug', 'slug')