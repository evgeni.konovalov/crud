import shutil, os, random

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.template.defaultfilters import slugify

from faker import Faker

from webapp.models import Category, Item

CATEGORIES_COUNT = 3
ITEM_MEDIA_PATH = '/webapp/item/picture'

class Command(BaseCommand):
    help = 'Seeds db with demo data'

    def handle(self, *args, **options):

        fake = Faker()

        for i in range(CATEGORIES_COUNT):
            name = fake.word()
            category = Category(
                name = name, 
                slug = slugify(name)
            )
            category.save()

        os.system('cp -R %s %s' % (settings.BASE_DIR + '/demo_data/media/*', settings.MEDIA_ROOT))
        for filename in os.listdir(settings.MEDIA_ROOT + ITEM_MEDIA_PATH):
            picture = ITEM_MEDIA_PATH + '/' + filename
            name = ' '.join(fake.words(nb=random.randint(1,6)))
            category = Category.objects.all()[random.randint(0, Category.objects.count() - 1)]
            item = Item(
                name = name,
                slug = slugify(name),
                price = 100 * random.randint(1,21),
                description = fake.paragraph(nb_sentences=random.randint(3,8)),
                category = category,
                picture = picture
            )
            item.save()