from django.db import models

from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    slug = models.SlugField(blank=True, verbose_name=_('slug'))
    class Meta:
        ordering = ('name',)
        verbose_name = _('category')
        verbose_name_plural = _('categories')
    def __str__(self):
        return self.name

class Item(models.Model):
    picture = models.ImageField(upload_to='webapp/item/picture/')
    name = models.CharField(max_length=255, verbose_name=_('name'))
    slug = models.SlugField(blank=True, verbose_name=_('slug'))
    description = models.TextField(blank=True, null=False, default='', verbose_name=_('description'))
    price = models.PositiveIntegerField(null=False, blank=False, default=0, verbose_name=_('price'))
    category = models.ForeignKey(Category, blank=False, on_delete=models.CASCADE, null=False, default=0, verbose_name=_('category'))
    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')
    def __str__(self):
        return self.name
