"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import routers

from webapp.views import CategoryViewSet, ItemViewSet

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns_common = []
if settings.DEBUG:
    urlpatterns_static = static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns_media = static(settings.MEDIA_PATH, document_root=settings.MEDIA_ROOT)
    urlpatterns_common += urlpatterns_static + urlpatterns_media
urlpatterns += urlpatterns_common

router = routers.DefaultRouter()
router.register('categories', CategoryViewSet)
router.register('items', ItemViewSet)
urlpatterns += router.urls