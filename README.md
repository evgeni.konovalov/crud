# CRUD

This is an example of primitive microserviced so-called "Create Read Update Delete" web app made with ReactJS and common react tools like react-router, redux, redux-saga on the frontend and Django with rest-framework on the backend, developed and delivered with Docker and GitLab. It is made to display my skills 
and as a learning example for an anybody who needs it.

Demo: http://app.evgeni.konovalov.fvds.ru/

## Features

### Api

It is a **Django** app powered by **Django REST framework**. Made with a maximum out-of-the-box functions and a minimum configuration. Interesting feature of the api part is that it creates the demo data automatically when deployed, with the pictures from *api/app/demo_data/media/webapp/item/picture* directory (The pictures are Linux Mint wallpappers).

### Client 

It is a **ReactJS** app made with **redux**, **redux-saga**, **react-router**. Main feature of this app is a server side rendering, which is tricky to achieve when have an async preloading processes (like fetching categories or items from the api). The backend correctly sends 404 and 500 status pages when has this errors. All the side effects like fetching calls made with redux-saga package that uses concurency principles. It is great to use, but have the problem on the server side - if you have a sequence of async processes (load all the categories then load the item, for example) you can't just dispatch the common redux-saga END action to end the sequence because it will end the current sequence stage and will prevent the next stages from running. To solve this problem I made a kind of soft ending mechanism, see [store/sagas/gentle-end.js](https://gitlab.com/evgeni.konovalov/crud/blob/master/client/app/store/sagas/gentle-end.js) and [store/sagas/category.js](https://gitlab.com/evgeni.konovalov/crud/blob/master/client/app/store/sagas/category.js) as sequence example.

## Run locally

You must have docker and docker-compose installed.

```
git clone git@gitlab.com:evgeni.konovalov/crud.git
cd crud
docker-compose -f docker-compose.run-only.yml up
```

And go to [localhost](http://localhost)

### Editing

To edit the code you must omit the `-f docker-compose.run-only.yml` flag when run docker-compose. So you must simply use `docker-compose up` command.

#### Api

Runs with the `runserver` command in the development environment and restarts automatically when the files change.

#### Client

To edit the client app, before running `docker-compose up` command, go to *client/app* directory, remove old *build* directory with `sudo` if have one and run `npm run-script dev:build` command:
```
cd client/app
sudo rm -rf build
npm run-script dev:build
```
It will run a webpack building in the watch mode with livereload webpack plugin (I refused HMR and webpack-dev-server because of it's complexity when use a server side rendering)

Then open a new terminal, go to the project root directory and run
```
# (cd [project root]/)
docker-compose up
```

## Contact me

I'm a frontend developer with nice backend skills. Text me or mail:

Whatsapp: [evgeni konovalov](https://wa.me/79995684023)

Telegram: [@evgenynikolaevich](tg://resolve?domain=evgenynikolaevich)

Email: [evgeni.konovalov@gmail.com](mailto:evgeni.konovalov@gmail.com)